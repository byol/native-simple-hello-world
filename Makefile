# Makefile for the native-simple-hello-world sample
# It
# 1. builds libfcgi
# 2. creates the output directory /bin
# 3. builds the actual application and statically links libfcgi (alternatively, install libfcgi and link it dynamically)
# 4. copies the indicator file to the output directory.
all:
	cd ./lib/fcgi-2.4.1-SNAP-0311112127; ./configure; make
	mkdir -p ./bin
	g++ src/helloworld.cpp -I./lib/fcgi-2.4.1-SNAP-0311112127/include ./lib/fcgi-2.4.1-SNAP-0311112127/libfcgi/.libs/libfcgi.a ./lib/fcgi-2.4.1-SNAP-0311112127/libfcgi/.libs/libfcgi++.a -o bin/helloworld
	cp src/nativeContainer ./bin
