# native-simple-hello-world
A simple "hello world" XSA application demonstrating the usage of the xsa native container. This example contains:

1. The library `libfcgi` from http://www.fastcgi.com/drupal/node/5
2. A stripped version of the ["libfcgi echo-cpp.cpp example"](http://www.fastcgi.com/devkit/examples/echo-cpp.cpp) published [in this tutorial](http://chriswu.me/blog/writing-hello-world-in-fcgi-with-c-plus-plus/)

# Requirements

An XSA native application

1. has to provide an executable binary
2. has to specify the name of the binary in file named `nativeContainer` in the application's root directory
2. receives requests via the fastcgi protocol (see [Further Reading](#Further Reading)). Therefore, it has to provide a fast cgi server. This example uses the fcgi server provided by `libfcgi`.

# Build

Before deploying the application to the XSA Runtime, the application has to be built using gnumake:

1. Clone the example to `[example-install-dir]`
2. In a Linux shell, execute
<pre>
cd [example-install-dir]
make
</pre>

This builds the application and libfcgi, which is statically linked in this example.

The build result is written to directory `[example-install-dir]/bin`.

# Deploy

### On XSA on-premise

When deploying you're app, you have to specify the buildpack explicitely since it's not shipped with the XSA Runtime, yet:

<pre>
cd [example-install-dir]/bin
xs push native-hello-world -b "git://github.wdf.sap.corp/xs2/native-container-buildpack.git"
</pre>

Alternatively, you can clone and install the [native container buildpack](https://github.wdf.sap.corp/xs2/native-container-buildpack) before pushing the app:

<pre>
xs create-buildpack native-container-buildpack [path-to-native-container-buildpack] 0
cd [example-install-dir]/bin
xs push native-hello-world 
</pre>

### On CloudFoundry

On CloudFoundry, you don't have access to the corporate GitHub. Therefore, specify the following repository URL, which contains a clone of the native container buildpack (will not be necessary any more once the buildpack is released and officially installed):

<pre>
cd [example-install-dir]/bin
cf push native-hello-world -b "https://native-container-buildpack.cfapps.sap.hana.ondemand.com/git/native-container-buildpack"
</pre>

# Use the application

Find out the URL of the application:

<pre>
$ xs apps

Getting apps in org "myorg" / space "PROD" as XSMASTER...
Found apps:

name               requested state   instances   memory    disk          urls
---------------------------------------------------------------------------------------------------------------
native-hello-world STARTED           1/1         1.00 GB   <unlimited>   https://mo-xxxxxxxxx.mo.sap.corp:51004
</pre>

Navigate to https://mo-xxxxxxxxx.mo.sap.corp:51004 with your browser and you should see a "Hello World" welcome page.

# Sources

The sources for the example can be found in

<pre>
[example-install-dir]/src
</pre>

Please refer to http://www.fastcfgi.com for more information about the usage of the fcgi library.

# Further Reading

1. Buildpack: https://github.wdf.sap.corp/xs2/native-container-buildpack/blob/master/README.md
2. Architecture: https://wiki.wdf.sap.corp/wiki/pages/viewpage.action?pageId=1745757000
3. FastCGI home: http://fastcgi.com
4. FastCGI tutorial: http://chriswu.me/blog/writing-hello-world-in-fcgi-with-c-plus-plus/




